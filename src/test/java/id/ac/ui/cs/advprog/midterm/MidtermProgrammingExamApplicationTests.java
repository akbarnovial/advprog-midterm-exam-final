package id.ac.ui.cs.advprog.midterm;

import id.ac.ui.cs.advprog.midterm.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MidtermProgrammingExamApplicationTests {
	@Autowired
	private UserController controller;

	@Test
	void contextLoads() {
		MidtermProgrammingExamApplication.main(new String[]{});
		assertThat(controller).isNotNull();
}

}
