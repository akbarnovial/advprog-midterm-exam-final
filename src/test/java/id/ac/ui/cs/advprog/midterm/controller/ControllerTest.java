package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.entity.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ControllerTest {
    private String id;
    private User user;

    @Autowired
    private MockMvc mockmvc;

    @MockBean
    private UserRepository repo;

    @BeforeEach
    public void testSignUp(){
        user = new User();
        user.setId(1806173595);
        user.setName("Akbar mantap");
        user.setEmail("akbarmantap@gmail.com");
        id = Long.toString(user.getId());
        when(repo.findById(user.getId()))
                .thenReturn(java.util.Optional.ofNullable(user));
    }

    @Test
    public void getsCorrectTemplateForSignUp() throws Exception {
        mockmvc.perform(get("/signup")).andExpect(status().isOk()).andExpect(view().name("add-user"));
    }
}
