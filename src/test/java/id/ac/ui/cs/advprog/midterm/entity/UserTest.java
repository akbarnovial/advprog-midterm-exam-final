package id.ac.ui.cs.advprog.midterm.entity;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.assertj.core.api.Assertions.assertThat;

public class UserTest {
    private User user;

    @BeforeEach
    public void signUp() {
        user = new User();
    }

    @Test
    public void getName_correct()
    {
        User user = new User("Rem", "rem@heart.com");
        assertThat(user.getName()).isEqualTo("Rem");
    }

    @Test
    public void setName_Correct() {
        User user = new User("Rem", "rem@heart.com");
        user.setName("Emilia");
        assertThat(user.getName()).isEqualTo("Emilia");
    }

    @Test
    public void getEmail_Correct() {
        User user = new User("Rem", "rem@heart.com");
        assertThat(user.getEmail()).isEqualTo("rem@heart.com");
    }

    @Test
    public void setEmail_Correct() {
        User user = new User("Rem", "rem@heart.com");
        user.setEmail("emilia@nextheart.com");
        assertThat(user.getEmail()).isEqualTo("Emilia@nextheart.com");
    }
}
